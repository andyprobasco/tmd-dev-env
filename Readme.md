# Current steps to deploy:







- add volumes:
    - `nodebb/volumes/nodebb-uploads.rar`
    - `mongo/volumes/mongo-data.rar`
    - `ghost/volumes/ghost-data.rar`
- add custom plugins
    - `nodebb/plugins/nodebb-theme-themanadrain`
- create `docker-compose.override.yml` with environment specific data, e.g.
```
version: "3.6"
services:
  nodebb:
    environment:
      - mongo__username=db_username
      - mongo__password=db_password
      - secret=secret
      - url=(enviornment url)
```
- be sure `nginx/nginx.conf` is pointing to the correct url for your environment.

- if testing locally, add `tmd-dev-env.com` to hosts file to make urls work correctly.

# ToDo:
- limit environment configuration to one file
- provide default, non-production `volume.rar` files
- provide ability to create `volume.rar` file from a `mongodump`
